// Setup Server
const express = require ('express');
const mongoose = require ('mongoose');
// by default our backend's CORS setting will prevent any application outside our Express JS app to process the request. Cors package will allow us to manipulate this and control what applications may use our app

// bridge front end backend
// allows backend application to be available to our frontend application and control the app's cross origin
const cors = require ('cors');


const userRoutes = require('./Routes/userRoutes.js')
const courseRoutes = require('./Routes/courseRoutes.js')

const port = 3001;

const app = express();



// Object Document Manager - ODM/
mongoose.set('strictQuery', true),
mongoose.connect('mongodb+srv://admin:admin@batch245-esmade.c4tetod.mongodb.net/batch245_Course_API_Esmade?retryWrites=true&w=majority',
		{
			// to avoid error/impact to future changes while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true
		})


let db = mongoose.connection

db.on('error', console.error.bind(console,'Connection Error'))
db.once('open', ()=> {console.log('We are now connected to the cloud!')})



// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());

// Routing
app.use('/user', userRoutes);
app.use('/course', courseRoutes);


app.listen(port,()=> console.log(`Server is running at port ${port}`));
