//[SECTION] Bcrypt
	// Installation:  npm i bcrypt

	// package to encrypt data/information when user register to our website
	

	/*Syntax:
		bcrypt.hashSync(password, saltRounds)*/

		// saltRounds is random value string that makes the hash unpredictable


//Example: password: bcrypt.hashSync(input.password, 10)





//[SECTION] JWT - JSON Web Token
	// Installation: npm i jsonwebtoken
	// Next Steps:
		/*
			-create auth.js and require jsonwebtoken
		*/

	//  object that securely transmits information between parties in a compact and self contained way
	//is an industry standard for sending information between our application in a secure manner
	// allows us to gain access to methods that will help us create JSON web token

	/*
	Anatomy
		-Header
		-Payload
		-Signature

	*/





