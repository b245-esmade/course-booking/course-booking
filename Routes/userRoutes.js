const express = require ('express')
const router = express.Router()

const auth = require('../auth.js')


const userController = require('../Controllers/userController.js')




//Routes
// Get all data
router.get('/', userController.getAllData)


// User Registration
router.post('/register', userController.userRegistration)


// User Authentication
router.post('/login', userController.userAuthentication)


// Retrieve data of specific user
router.get('/details', auth.verify, userController.getProfile);


//User Enrollment

router.post('/enroll/:courseId', auth.verify, userController.enrollCourse)

module.exports = router;



