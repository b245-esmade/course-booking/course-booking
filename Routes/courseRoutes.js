const express = require("express");
const router = express.Router();
const courseController = require("../Controllers/courseController");
const auth = require("../auth.js");

// Route for creating a course
router.post('/', courseController.addCourse);


// Admin Authentication
router.post('/', auth.verify, courseController.addCourse)

// All Courses
router.get('/all', auth.verify, courseController.allCourses)


// Retrieve ALL active Courses -not required to verify
router.get('/allActive', courseController.allActiveCourses)


 
// Retrieve Inactive Courses

router.get('/allInactive', auth.verify, courseController.allInactiveCourses)


// Get Specific Course (w/ Params)

router.get('/:courseId', courseController.courseDetails )


// Update Specific Course (w/ Params)

router.put('/update/:courseId', auth.verify, courseController.updateCourse)


// Archive Course

router.patch('/update/:courseId/archive', auth.verify, courseController.updateStatus)


module.exports = router;
