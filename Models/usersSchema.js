const mongoose = require ('mongoose')

const usersSchema = new mongoose.Schema({
		firstName: {
				type: String,
				required: [true, 'The FIRST NAME is required']
			},
		lastName: {
				type: String,
				required: [true, 'The LAST NAME is required']
			},
		email: {
				type: String,
				required: [true, 'The EMAIL ADDRESS is required']
			},
		password: {
				type: String,
				required: [true, 'You need to set up a PASSWORD!']
			},
		isAdmin: {
				type: Boolean,
				default: false
			},
		mobileNo: {
				type:Number,
				required: [true, 'The MOBILE NUMBER is required']
			},
		enrollments: [
			{
				courseId: {
					type: String,
					required: [true, 'The COURSE ID is required']
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
				type: String,
				default: 'Enrolled'
				}
			}
		]

})


module.exports = mongoose.model('Users', usersSchema);