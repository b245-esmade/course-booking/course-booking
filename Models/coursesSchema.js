const mongoose = require('mongoose');

const coursesSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'NAME OF THE COURSE is required']
	},
	description: {
		type: String,
		required: [true, 'DESCRIPTION of the course is required']
	},
	price: {
		type:Number,
		required: [true, 'PRICE of the course is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date() //"instantiates new date that stores the current date/time whenever a course is created in our DB"
	},
	enrollees: [
	{
		userId: {
			type: String,
			required: [true, 'USER ID  is required']
		},
		enrolledOn: {
			type: Date,
			default: new Date()
		}
	}

	]

})



module.exports = mongoose.model('Course', coursesSchema);