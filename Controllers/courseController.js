const mongoose = require('mongoose')
const Course = require("../Models/coursesSchema.js");
const auth = require('../auth.js')

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request,response) =>{
    const verifyAdmin = auth.decode(request.headers.authorization);
    console.log(verifyAdmin);

    if(verifyAdmin.isAdmin === true){
        let input = request.body;
        let newCourse = new Course({
            name : input.name,
            description: input.description,
            price: input.price
        })
        return newCourse.save()
        .then(result => {
            console.log(newCourse);
            return response.send("Course successfully added!");
        })
        .catch(error =>{
            console.log(error);
            return response.send(false);
        })
    }
    else{
        return response.send("You are not authorized to do this action. Please contact the administrator!")
    }
}


// Get All Courses

module.exports.allCourses = (request,response) => {
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);

    if (!userData.isAdmin){
        return response.send("You don't have access to this route")
    }else{
        Course.find({})
        .then(result => response.send(result))
        .catch(error => response.send(error));
    }


}

// Retrieve Active Courses
module.exports.allActiveCourses = (request,response) => {

    Course.find({isActive: true})
    .then(result => response.send(result))
    .catch(error => response.send(error))

}





// Retrieve Inactive Courses

module.exports.allInactiveCourses = (request,response) => {

     const userData = auth.decode(request.headers.authorization);

     if (!userData.isAdmin){
        return response.send("You don't have access to this route")
     }else{
        Course.find({isActive:false})
        .then(result => response.send(result))
        .then(error => response.send(error))
     }
}


// Retrieve Specific Course (using ID)


module.exports.courseDetails = (request, response) => {
    const courseId = request.params.courseId;

    Course.findById(courseId)
    .then(result => response.send(result))
    .catch(error => response.send(error))
}



// Update the course in Params

module.exports.updateCourse = async (request,response) => {
    const userData = auth.decode(request.headers.authorization)

    const courseId = request.params.courseId;

    const input = request.body;

    if(!userData.isAdmin){
        return response.send("You don't have access to this route")
    }else {
        await Course.findOne({_id: courseId})
        .then(result => {
            if (result === null){
                return response.send('Invalid Course ID. Please try again!')
            }else {
                let updatedCourse = {
                    name: input.name,
                    description: input.description,
                    price: input.price
                }
                Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
                .then(result => {
                    console.log(result)
                    response.send (result)})
                .catch(error => response.send (error))
            }
        })
        // .catch(error => response.send(error))
    }
}



//Archive course (Set to Inactive)

module.exports.updateStatus = (request,response) => {
    const userData = auth.decode(request.headers.authorization)

    const courseId = request.params.courseId;

    const input = request.body;

    if(!userData.isAdmin){
        return response.send("You don't have access to this route")
    }else {
         Course.findOne({_id: courseId})
        .then(result => {
            if (result === null){
                return response.send('Invalid Course ID. Please try again!')
            }else {
                let updatedStatus = {
                    // name: input.name,
                    // description: input.description,
                    // price: input.price,
                    isActive: input.isActive
                }
                Course.findByIdAndUpdate(courseId, updatedStatus, {new: true})
                .then(result => {
                    console.log(result)
                    response.send (result)})
                .catch(error => response.send (error))
            }
        })
        // .catch(error => response.send(error))
    }
}