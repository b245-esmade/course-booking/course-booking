const mongoose = require('mongoose')

const User = require('../Models/usersSchema.js')
const Course = require ('../Models/coursesSchema.js')

const bcrypt = require('bcrypt')

const auth = require('../auth.js')







// Controllers & Functions

// GET all data

module.exports.getAllData = (request,response) => {

	User.find({})
	.then(result =>{
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}



// CREATE or REGISTER a user on our db/database

module.exports.userRegistration = (request,response) => {
	const input = request.body;


// To ensure USER is unique upon Registration
	User.findOne({email: input.email})
	.then(result => {
		if (result !== null){
			return response.send('The email already exists')
		}else {
			let newUser = new User ({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send('You are now registered!')
			})
			.catch(error => {
				return response.send(error)
			})

		}
	})
	.catch(error => {
		return response.send(error)
	})

}


//User Authentication
module.exports.userAuthentication = (request, response) => {
		let input = request.body;

		//Possible scenarios in logging in
			//1. email is not yet registered.
			//2. email is registered but the password is wrong

		User.findOne({email: input.email})
		.then(result => {
			if(result === null){
				return response.send("Email is not yet registered. Register first before logging in!")
			}else{
				// we have to verify if the passwor is correct
				// The "compareSync" method is used to compare a non encrypted password to the encrypted password.
				//it returns boolean value, if match true value will return otherwise false.
				const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

				if(isPasswordCorrect){
					return response.send({auth: auth.createAccessToken(result)});
				}else{
					return response.send("Password is incorrect!")
				}

			}




		})
		.catch(error => {
			return response.send(error);
		})

}

// Retrieve the user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the result document to an empty string("Confidential").
	3. Return the result back to the frontend
*/

module.exports.getProfile = (request, response) => {
	// let input = request.body;
	const userData = auth.decode(request.headers.authorization);

	console.log(userData);

	return User.findById(userData._id)
	.then(result =>{
		// avoid to expose sensitive information such as password.
		result.password = "Confidential";

		return response.send(result);
	})
}



// User Enrollment
	// We can get the ID of the user by decoding the JWT
	// We can get the courseId by using the request params

module.exports.enrollCourse = (request,response) =>{
	// First we have to get user Id and CourseId
	// decode the token to extract payload
	const userData = auth.decode(request.headers.authorization);

	// get course Id
	const courseId = request.params.courseId

	// 2things
	/*
	1.Push courseId in the enrollment property of the user
	2.Push the userId in the enrollees property of the course*/


	if(!userData.isAdmin){

		Course.findById(courseId)
		.then(result => {
			if (result === null) {
				response.send('Invalid Course ID. Please try again!')
			}else {
				let isUserUpdated = User.findById(userData._id)

				.then(result => {
					if (result === null){
						return false
					}else {
						result.enrollments.push({courseId: courseId})
						
						return result.save()
						.then(save => true)
						.catch(error => false)

					}
				})

				let isCourseUpdated = Course.findById(courseId)
				.then(result => {
					if(result === null){
						return false;
					}else{
						result.enrollees.push({userId: userData._id})

						return result.save()
						.then(save => true)
						.catch(error => false)
					}
				})

				if(isCourseUpdated && isUserUpdated){
					return response.send('The course is now enrolled')
				}else {
					return response.send("There was an error during the enrollment. Please try again")
				}

			}

		})
		.catch(error => response.send(error))

	} else {
		response.send('Enrollment Denied')

	}





	


	

	
}
